import {drag} from "d3-drag";
import { select } from "d3-selection";


export function network_drag(simulation) {
  return function mydrag(simulation,node){drag().on("start",dragstart(node)).on("drag",click(node)).on("end",dragended(node))(select(simulation))};
  function dragstart(node,{event}){
    if (!event.active) simulation.alphaTarget(0.3).restart();
      node.fx = event.subject.x;
      node.fy = event.subject.y;
  }
  function dragended(node,{event}){
    if (!event.active) simulation.alphaTarget(0);
    node.fx = null;
    node.fy = null;
  }
  function click(node,{event}){
    node.fx = event.x;
    node.fy = event.y;
  }
}
