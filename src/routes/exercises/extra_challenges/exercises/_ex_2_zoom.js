import {select} from "d3-selection";
import {zoom,zoomIdentity} from "d3-zoom";

export function zooming(width, height) {
  select(element).call(zoom()
  .scaleExtent([1, 8])
  .on("zoom", ({transform}) => zoomed(transform)));
  function zoomed(transform) {
    element.save();
    element.clearRect(0, 0, width, height);
    element.translate(transform.x, transform.y);
    element.scale(transform.k, transform.k);
    element.beginPath();
    for (const [x, y] of data) {
      element.moveTo(x + r, y);
      element.arc(x, y, r, 0, 2 * Math.PI);
    }
    element.fill();
    element.restore();
  }
  zoomed(zoomIdentity);
  return element.canvas;
}
